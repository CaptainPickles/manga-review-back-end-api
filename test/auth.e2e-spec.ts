import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import {
    getConnectionToken,
    MongooseModule,
    MongooseModuleOptions,
} from '@nestjs/mongoose';
import { ConfigService } from '@nestjs/config';
import mongoose, { Connection, Mongoose } from 'mongoose';

describe('Auth Controller (e2e)', () => {
    let app: INestApplication;
    let connection: Mongoose;
    beforeAll(async () => {
        connection = await mongoose.connect(process.env.DATABASE_TEST_URL);
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [
                MongooseModule.forRootAsync({
                    useFactory: async (configService: ConfigService) =>
                        configService.get<MongooseModuleOptions>('database_test'),
                    inject: [ConfigService],
                }),
                AppModule,
            ],
        }).compile();
        app = moduleFixture.createNestApplication();
        app.useGlobalPipes(new ValidationPipe());
        await app.init();
    })


    afterAll(async () => {
        await app.close();
        await connection.connection.close()
    });

    it("/auth/login test login with no user (POST)", async () => {
        expect(true).toBe(true)
    })
});
