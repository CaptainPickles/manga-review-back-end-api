import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import {
  getConnectionToken,
  MongooseModule,
  MongooseModuleOptions,
} from '@nestjs/mongoose';
import { ConfigService } from '@nestjs/config';
import mongoose, { Connection, Mongoose } from 'mongoose';

describe('User Controller (e2e)', () => {
  let app: INestApplication;
  let connection: Mongoose;
  beforeAll(async () => {
    connection = await mongoose.connect(process.env.DATABASE_TEST_URL);
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRootAsync({
          useFactory: async (configService: ConfigService) =>
            configService.get<MongooseModuleOptions>('database_test'),
          inject: [ConfigService],
        }),
        AppModule,
      ],
    }).compile();
    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  })


  afterAll(async () => {
    await app.close();
    await connection.connection.close()
  });

  it('/users/register (POST)', async () => {
    //arrange 
    const fetchedEmptyDbUser = await connection.connection.db
      .collection('users')
      .findOne();
    expect(fetchedEmptyDbUser).toBe(null)

    //act
    await request(app.getHttpServer())
      .post('/users/register')
      .send({
        email: 'test@test.fr',
        password: 'Test1234',
        username: 'test',
      })
      .expect(201)
      .then(async (res) => {
        expect(res.body).toMatchObject({
          email: 'test@test.fr',
          username: 'test',
          isAdmin: false,
          createdAt: expect.any(Number),
          _id: expect.any(String),
          __v: 0,
        });

        // assert
        const fetchedUserFromDb = await connection.connection.db
          .collection('users')
          .findOne();
        expect(fetchedUserFromDb).toHaveProperty('password');
        expect(fetchedUserFromDb.password).not.toEqual('Test1234');
        delete fetchedUserFromDb.password;
        expect(res.body).toMatchObject(fetchedUserFromDb);
      });
  });

  it('/users/register  email already used (POST)', () => {
    return request(app.getHttpServer())
      .post('/users/register')
      .send({
        email: 'test@test.fr',
        password: 'Test1234',
        username: 'test',
      })
      .expect(400)
  });

  it('/users/register  testing body dto not good format (POST)', async () => {
    const response = await request(app.getHttpServer())
      .post('/users/register')
      .send({
        email: 'test',
        password: 'a',
        username: 'a',
      })
      .expect(400)
    expect(response.body.message === ['password too weak',
      'password must be longer than or equal to 4 characters',
      'email must be an email',
      'username must be longer than or equal to 4 characters'])
  });

  it('/users/update  testing try update without being login (PATCH)', async () => {
    const response = await request(app.getHttpServer())
      .patch('/users/update')
      .send({
        email: 'test',
        username: 'a',
      })
      .expect(401)
  });
  it.todo("/users/update logged user update is data")

  it("/users/list testing listing of all users ", async () => {
    const response = await request(app.getHttpServer())
      .get('/users/list')
      .expect(200)

    expect(response.body).toHaveLength(1)
    expect(response.body[0].email).toBe("test@test.fr")
  })

  it("/users/:id testing provide wrong id not mongoose format ", async () => {
    const response = await request(app.getHttpServer())
      .get('/users/id/notGoodFormatId')
      .expect(400)

    expect(response.body.message).toEqual("id not good format")
  })

  it("/users/:id testing fetching user by id ", async () => {
    const fetchedEmptyDbUser = await connection.connection.db
      .collection('users')
      .findOne();
    const response = await request(app.getHttpServer())
      .get('/users/id/' + fetchedEmptyDbUser._id)
      .expect(200)
    delete fetchedEmptyDbUser.password
    expect(response.body).toMatchObject(fetchedEmptyDbUser)
    expect(response.body).not.toHaveProperty("password")
  })
});
