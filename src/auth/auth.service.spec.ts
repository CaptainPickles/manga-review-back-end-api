import { JwtService } from '@nestjs/jwt';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { User, UserDocument } from '../users/entities/user.entity';
import { UsersService } from '../users/users.service';
import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;
  let fakeUserService: Partial<UsersService>;
  const userSeededInDb: User = {
    email: 'test@test.fr',
    password:
      'ca8e643748acb5ba.94e622aca113c37756693b0ccbe5014d5474d122733ac900d8492bf285153ec5', // hashed and salted Test123
    username: 'test',
    createdAt: Date.now(),
    isAdmin: false,
    description: 'description de test',
  };
  beforeAll(async () => {
    const fakeBddUsers: User[] = [userSeededInDb];
    fakeUserService = {
      create: (createUserDto: CreateUserDto) => {
        return Promise.resolve({
          _id: 'testId',
          email: createUserDto.email,
          username: createUserDto.username,
        });
      },
      findByEmail: (email: string) => {
        return Promise.resolve(
          fakeBddUsers.find((user) => user.email === email) as UserDocument,
        );
      },
    };
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        { provide: UsersService, useValue: fakeUserService },
        JwtService,
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  it('throws if signin is called with a non existing email', async () => {
    await expect(
      service.validateUser('asf@gmail.com', 'blabla'),
    ).rejects.toThrowError('user not found');
  });

  it('throws if signin is called with a existing email but bad password', async () => {
    await expect(
      service.validateUser('test@test.fr', 'Wrong password'),
    ).rejects.toThrowError('bad password');
  });

  it('Should validate user if good email and password provided', async () => {
    const user = await service.validateUser(userSeededInDb.email, 'Test123');
    expect(user).toMatchObject(userSeededInDb);
  });
});
