import { IsEmail, IsNumber, IsString } from 'class-validator';
/**
 * allow login of a user with email and password
 * Only used for transport
 */
export class LoginDto {
   /**
* email of the user
* @example Jimmy216@gmail/com
*/
   @IsString()
   @IsEmail()
   email: string;

   /**
* password of the user
* @example test2155Dsz
*/
   @IsString()
   password: string;

}
