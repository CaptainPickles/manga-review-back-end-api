import {
  Controller,
  Request,
  Post,
  UseGuards,
  Get,
  Body,
  Response,
  Param,
} from '@nestjs/common';
import { ApiBadRequestResponse, ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './authGuards/jwt-auth.guard';
import { LocalAuthGuard } from './authGuards/local-auth.guard';
import { LoginDto } from './dto/login.dto';

@ApiTags('auth')
@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService) { }
  /**
   * login with email and password returning bearer token
   * @param req
   * @param body
   * @returns
   */
  @UseGuards(LocalAuthGuard)
  @Post('/login')
  @ApiResponse({
    status: 201,
    description: 'the connexion has been established.',
    headers: { "Authortisation": { description: "Bearer id used as JWT token expire in 1 day" } }
  })
  @ApiBadRequestResponse({
    status: 404,
    description: 'the email not correspond to any user.',
  })
  @ApiResponse({
    status: 401,
    description: 'the password is incorrect.',
  })
  async login(@Request() req, @Body() body: LoginDto, @Response() res) {
    const token = await this.authService.login(req.user);
    return res
      .set({ Authorization: 'Bearer ' + token.access_token })
      .json(token.user);
  }
  /**
   * Display all information related about the user connected
   */
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get('user-info')
  getUserInfo(@Request() req) {
    return req.user;
  }
}
