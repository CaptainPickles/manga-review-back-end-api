import {
  UnauthorizedException,
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Manganime, ManganimeDocument } from './entities/manganime.entity';
import { Comment, CommentDocument } from '../comments/entities/comment.entity';
import { GenreManganime, GenreManganimeDocument, Genre, GenreDocument } from '../genres/entities/genre.entity';
import { UserDocument } from '../users/entities/user.entity';
import { CreateManganimeDto } from './dto/create-manganime.dto';
import { UpdateManganimeDto } from './dto/update-manganime.dto';
import { Review, ReviewDocument } from '../reviews/entities/review.entity';

/**
 * This class is the manganime service to handle all manganime methods
 */
@Injectable()
export class ManganimeService {
  constructor(@InjectModel(Manganime.name) private manganimeModel: Model<ManganimeDocument>,
    @InjectModel(GenreManganime.name) private genremanganimeModel: Model<GenreManganimeDocument>,
    @InjectModel(Genre.name) private genreModel: Model<GenreDocument>,
    @InjectModel(Comment.name) private commentModel: Model<CommentDocument>,
    @InjectModel(Review.name) private reviewModel: Model<ReviewDocument>) { }

  /**
  * This method handle the creation of a manganime
  */
  async create(user: UserDocument, createManganimeDto: CreateManganimeDto): Promise<Partial<Manganime>> {
    createManganimeDto['authorId'] = user._id;
    const manganimeToCreate = new this.manganimeModel(createManganimeDto);
    const createdManganime = await manganimeToCreate.save();
    const manganimeToReturn = createdManganime.toJSON();
    for (let genreId in createManganimeDto["genres"]) {
      let id = createManganimeDto["genres"][genreId];
      if (!id.match(/^[0-9a-fA-F]{24}$/)) continue;

      let genre = await this.genreModel.findById(id);
      if (genre == null) continue;

      new this.genremanganimeModel({ "genreId": id, "manganimeId": manganimeToReturn._id }).save();
    }
    return manganimeToReturn;
  }

  /**
  * This method returns all the manganimes
  */
  async findAll() {
    const manganimesToReturn = await this.manganimeModel.find().populate('authorId', '-password').lean();
    for (let manganime of manganimesToReturn) {
      manganime['genres'] = await this.genremanganimeModel.find({ manganimeId: manganime._id.toString() }).populate('genreId');
      manganime['comments'] = await this.commentModel.find({ manganimeId: manganime._id.toString() }).populate('authorId', '-password');
      manganime['reviews'] = await this.reviewModel.find({ manganimeId: manganime._id.toString() }).populate('authorId', '-password');
    }
    return manganimesToReturn;
  }

  /**
  * This method returns all manganime from author id
  */
  async findAllByAuthor(id: string) {
    const manganimesToReturn =  await this.manganimeModel.find({authorId: id}).populate('authorId', '-password').lean();
    if (manganimesToReturn == null) throw new BadRequestException("The requested manga does not exist.");
    for(let manganime of manganimesToReturn) {
      manganime['genres'] = await this.genremanganimeModel.find({manganimeId: manganime._id.toString()}).populate('genreId');
      manganime['comments'] = await this.commentModel.find({manganimeId: manganime._id.toString()}).populate('authorId');
      manganime['reviews'] = await this.reviewModel.find({ manganimeId: manganime._id.toString() }).populate('authorId');
    }
    return manganimesToReturn;
  }
  

  /**
  * This method returns one manganime
  */
  async findOne(id: string) {
    const manganimeToReturn = await this.manganimeModel.findById(id).populate('authorId', '-password').lean();
    if (manganimeToReturn == null) throw new BadRequestException("The requested manga does not exist.");
    manganimeToReturn["genres"] = await this.genremanganimeModel.find({ manganimeId: manganimeToReturn._id.toString() }).populate('genreId');
    manganimeToReturn['comments'] = await this.commentModel.find({ manganimeId: manganimeToReturn._id.toString() }).populate('authorId', '-password');
    manganimeToReturn['reviews'] = await this.reviewModel.find({ manganimeId: manganimeToReturn._id.toString() }).populate('authorId', '-password');
    return manganimeToReturn;
  }

  /**
  * This method handle the update of a manganime
  */
  async update(user: UserDocument, id: string, updateManganimeDto: UpdateManganimeDto) {
    const checkManganime =  await this.manganimeModel.findById(id);
    if(checkManganime == null) throw new BadRequestException("The requested manga does not exist.");

    if(!user.isAdmin && user._id !== checkManganime.authorId) throw new UnauthorizedException("You're not authorized to do that.");
    const manganimeToReturn = await this.manganimeModel.findByIdAndUpdate({ _id: id }, updateManganimeDto, { new: true })
    if (manganimeToReturn == null) throw new BadRequestException("The requested manga does not exist.");
    await this.genremanganimeModel.deleteMany({ manganimeId: id.toString() })
    await this.commentModel.deleteMany({ manganimeId: id.toString() })

    for (let genreId in updateManganimeDto["genres"]) {
      let id2 = updateManganimeDto["genres"][genreId];
      if (!id2.match(/^[0-9a-fA-F]{24}$/)) continue;

      let genre = await this.genreModel.findById(id2);
      if (genre == null) continue;

      new this.genremanganimeModel({ "genreId": id2, "manganimeId": id }).save();
    }
    return manganimeToReturn;
  }

  /**
  * This method handle the deletion of a manganime
  */
  async remove(user: UserDocument, id: string): Promise<ManganimeDocument> {
    const checkManganime =  await this.manganimeModel.findById(id);
    if(checkManganime == null) throw new BadRequestException("The requested manga does not exist.");

    if(!user.isAdmin && user._id !== checkManganime.authorId) throw new UnauthorizedException("You're not authorized to do that.");
    
    const manganimeToReturn =  await this.manganimeModel.findByIdAndDelete(id)
    if (manganimeToReturn == null) throw new BadRequestException("The requested manga does not exist.");
    await this.genremanganimeModel.deleteMany({ manganimeId: id.toString() })
    await this.commentModel.deleteMany({ manganimeId: id.toString() })
    await this.reviewModel.deleteMany({ manganimeId: id.toString() })
    return manganimeToReturn;
  }
}
