import { PartialType } from '@nestjs/swagger';
import { CreateManganimeDto } from './create-manganime.dto';

/**
 * This class is used to update the manganime
 */
export class UpdateManganimeDto extends PartialType(CreateManganimeDto) {}
