import {
    IsDate,
    IsDateString,
    IsEmail,
    IsArray,
    IsNumber,
    IsString,
    Matches,
    MaxLength,
    MinLength,
  } from 'class-validator';
import { StringifyOptions } from 'querystring';

/**
 * This class is used to create / update the manganime
 */
export class CreateManganimeDto {
    /**
     * Name of the manganime
     * @example "Naruto"
     */
    @IsString()
    @MinLength(2)
    @MaxLength(100)
    name: string;

    /**
     * Description of the manganime
     * @example "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi hendrerit nunc arcu, rutrum elementum lacus porttitor et. Praesent turpis neque, iaculis a blandit non, convallis quis purus. Nullam in libero efficitur nulla fermentum imperdiet."
     */
    @IsString()
    @MinLength(10)
    @MaxLength(500)
    description: string;

    /**
     * type of the manganime
     * @example "manga"
     */
    @IsString()
    @MinLength(4)
    @MaxLength(20)
    type: string;

    /**
     * image url of the manganime
     * @example "https://image.example/azertyuiop.png"
     */
    @IsString()
    @MinLength(4)
    @MaxLength(2000)
    imageUrl: string;
    
    /**
     * id genres of the manganime
     * @example "[62bd57d9f1be9ce48b3c9fbc, 62bd57d9f1be9ce48b3c9fbf]"
     */
    @IsArray()
    genres: Array<String>;
}
