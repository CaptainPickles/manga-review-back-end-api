import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ManganimeDocument = Manganime & Document;

/**
 * This class is the manganime entity used for mongoose
 */
@Schema()
export class Manganime {
  /**
  * Name of the manganime
  * @example "Naruto"
  */
  @Prop()
  name: string;

  /**
  * Description of the manganime
  * @example "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi hendrerit nunc arcu, rutrum elementum lacus porttitor et. Praesent turpis neque, iaculis a blandit non, convallis quis purus. Nullam in libero efficitur nulla fermentum imperdiet."
  */
  @Prop()
  description: string;

  /**
  * type of the manganime
  * @example "manga"
  */
  @Prop()
  type: string;

  /**
  * image url of the manganime
  * @example "https://image.example/azertyuiop.png"
  */
  @Prop()
  imageUrl: string;

  /**
  * id of the author
  * @example "62bd57d9f1be9ce48b3c9fbc"
  */
  @Prop({ref: 'User'})
  authorId: string;

  /**
  * creation date timestamp of the manganime
  * @example 1656426188913
  */
  @Prop({ type: Number, default: Date.now() })
  createdAt: number;
}

export const ManganimeSchema = SchemaFactory.createForClass(Manganime);