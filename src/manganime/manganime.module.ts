import { Module } from '@nestjs/common';
import { ManganimeService } from './manganime.service';
import { ManganimeController } from './manganime.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Manganime, ManganimeSchema } from './entities/manganime.entity';
import { GenreManganime, GenreManganimeSchema, Genre, GenreSchema } from '../genres/entities/genre.entity';
import { Comment, CommentSchema } from '../comments/entities/comment.entity';
import { Review, ReviewSchema } from '../reviews/entities/review.entity';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Manganime.name, schema: ManganimeSchema }]),
    MongooseModule.forFeature([{ name: GenreManganime.name, schema: GenreManganimeSchema }]),
    MongooseModule.forFeature([{ name: Genre.name, schema: GenreSchema }]),
    MongooseModule.forFeature([{ name: Comment.name, schema: CommentSchema }]),
    MongooseModule.forFeature([{ name: Review.name, schema: ReviewSchema }])
  ],
  controllers: [ManganimeController],
  providers: [ManganimeService]
})
export class ManganimeModule { }
