/**
 * en variable for all project
 */
export default () => ({
  port: parseInt(process.env.PORT, 10) || 3001,
  database: {
    uri: process.env.DATABASE_URL,
  },
  database_test: {
    uri: process.env.DATABASE_TEST_URL,
  },
  db_test_name: process.env.DB_TEST_NAME,
  JWT_SECRET: process.env.JWT_SECRET,
});
