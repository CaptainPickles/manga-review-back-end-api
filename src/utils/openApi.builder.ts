import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

/**
 * Create document api created with nest decorator and ts comments
 * serve it on localhost/api
 * only create if Node_env is dev
 */
export function buildAndSetupDocument(app: INestApplication) {
    if (process.env.NODE_ENV !== 'dev') return;
    const documentBuilderConfig = new DocumentBuilder()
        .setTitle('Manga Review Back End Api')
        .setDescription('The manga review API description')
        .setVersion('1.0')
        .addTag('Manga Review')
        .addBearerAuth()
        .build();
    const document = SwaggerModule.createDocument(app, documentBuilderConfig);
    SwaggerModule.setup('api', app, document);
}