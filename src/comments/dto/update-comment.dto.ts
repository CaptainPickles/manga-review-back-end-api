import { PartialType } from '@nestjs/swagger';
import { CreateCommentDto } from './create-comment.dto';

/**
* This class is used to update the comment
*/
export class UpdateCommentDto extends PartialType(CreateCommentDto) {}
