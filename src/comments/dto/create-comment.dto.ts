import {
  IsDate,
  IsDateString,
  IsEmail,
  IsArray,
  IsNumber,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';
import { StringifyOptions } from 'querystring';

/**
* This class is used to create / update the comment
*/
export class CreateCommentDto {

    /**
    * Id of the comment
    * @example "62bd57d9f1be9ce48b3c9fbg"
    */
    @IsString()
    manganimeId: string;

    /**
    * Content of the comment
    * @example "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi hendrerit nunc arcu, rutrum elementum lacus porttitor et. Praesent turpis neque, iaculis a blandit non, convallis quis purus. Nullam in libero efficitur nulla fermentum imperdiet."
    */
    @IsString()
    @MinLength(4)
    @MaxLength(300)
    content: string;

}
