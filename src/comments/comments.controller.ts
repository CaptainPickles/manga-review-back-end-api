import { UseGuards, Request, Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ApiResponse, ApiBearerAuth,ApiTags } from '@nestjs/swagger';
import { CommentsService } from './comments.service';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { JwtAuthGuard } from '../auth/authGuards/jwt-auth.guard';

/**
 * This class is the comment controller used to routing
 */
@ApiTags('comments')
@Controller('comments')
export class CommentsController {
  constructor(private readonly commentsService: CommentsService) {}

  /**
   * Post request to create a comment
   * @example [POST] https://example/comments
   */
  @ApiResponse({ status: 201, description: 'The comment has been successfully created.'})
  @ApiResponse({ status: 400, description: 'Bad Request'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Request() req, @Body() createCommentDto: CreateCommentDto) {
    return this.commentsService.create(req.user, createCommentDto);
  }

  /**
   * Get request to find all comments from authorId
   * @example [GET] https://example/comments/author/id
   */
  @ApiResponse({ status: 200, description: 'The comments has been successfully requested.'})
  @Get('/author/:id')
  findAllByAuthor(@Param('id') id: string) {
    return this.commentsService.findAllByAuthor(id);
  }

  /**
   * Patch request to update a comment by its Id
   * @example [PATCH] https://example/comments/id
   */
  @ApiResponse({ status: 200, description: 'The comment has been successfully updated.'})
  @ApiResponse({ status: 400, description: 'Bad Request'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  update(@Request() req, @Param('id') id: string, @Body() updateCommentDto: UpdateCommentDto) {
    return this.commentsService.update(req.user, id, updateCommentDto);
  }

  /**
   * Delete request to delete a comment by its Id
   * @example [DELETE] https://example/comments/id
   */
  @ApiResponse({ status: 200, description: 'The comment has been successfully deleted.'})
  @ApiResponse({ status: 400, description: 'Bad Request'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Request() req, @Param('id') id: string) {
    return this.commentsService.remove(req.user, id);
  }
}
