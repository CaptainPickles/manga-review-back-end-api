import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import helmet from 'helmet';
import { ConfigService } from '@nestjs/config';
import { buildAndSetupDocument } from './utils/openApi.builder';
async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  app.enableCors({ exposedHeaders: ["Authorization"] })
  app.use(helmet());
  buildAndSetupDocument(app);
  app.useGlobalPipes(new ValidationPipe({ whitelist: true })); // enable validation pipes isString ...

  const configService = app.get(ConfigService);
  const port = configService.get('port');
  await app.listen(port);
}
bootstrap();
