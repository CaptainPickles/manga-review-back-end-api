import {
  BadRequestException,
  UnauthorizedException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Genre, GenreDocument, GenreManganime, GenreManganimeDocument } from './entities/genre.entity';
import { UserDocument } from '../users/entities/user.entity';
import { CreateGenreDto } from './dto/create-genre.dto';
import { UpdateGenreDto } from './dto/update-genre.dto';

/**
 * This class is the genre service to handle all genres methods
 */
@Injectable()
export class GenresService {
  constructor(@InjectModel(Genre.name) private genreModel: Model<GenreDocument>, @InjectModel(GenreManganime.name) private genremanganimeModel: Model<GenreManganimeDocument>) { }

  /**
  * This method handle the creation of a genre
  */
  async create(user: UserDocument, createGenreDto: CreateGenreDto): Promise<Partial<Genre>> {
    if(!user.isAdmin) throw new UnauthorizedException("You're not authorized to do that.");
    const genreToCreate = new this.genreModel(createGenreDto);
    const createdGenre = await genreToCreate.save();
    const genreToReturn = createdGenre.toJSON();
    return genreToReturn;
  }

  /**
  * This method returns all the genres
  */
  async findAll() {
    const genresToReturn = await this.genreModel.find().lean();
    for(let genre of genresToReturn) genre['manganimes'] = await this.genremanganimeModel.find({genreId: genre._id.toString()}).populate('manganimeId');
    return genresToReturn;
  }

  /**
  * This method returns one genre
  */
  async findOne(id: string) {
    const genreToReturn =  await this.genreModel.findById(id).lean();
    if (genreToReturn == null) throw new BadRequestException("The requested manga does not exist.");
    genreToReturn["manganimes"] = await this.genremanganimeModel.find({genreId: id.toString()}).populate('manganimeId');
    return genreToReturn;
  }

  /**
  * This method handle the update of a genre
  */
  async update(user: UserDocument, id: string, updateGenreDto: UpdateGenreDto) {
    if(!user.isAdmin) throw new BadRequestException("You're not authorized to do that.");
    const genreToReturn = await this.genreModel.findByIdAndUpdate({ _id: id }, updateGenreDto, { new: true })
    if (genreToReturn == null) throw new BadRequestException("The requested manga does not exist.");
    return genreToReturn;
  }

  /**
  * This method handle the deletion of a genre
  */
  async remove(user: UserDocument, id: string) {
    if(!user.isAdmin) throw new BadRequestException("You're not authorized to do that.");
    const genreToReturn =  await this.genreModel.findByIdAndDelete(id)
    if (genreToReturn == null) throw new BadRequestException("The requested manga does not exist.");
    await this.genremanganimeModel.deleteMany({genreId: id.toString()})
    return genreToReturn;
  }
}
