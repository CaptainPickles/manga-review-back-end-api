import { Module } from '@nestjs/common';
import { GenresService } from './genres.service';
import { GenresController } from './genres.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Genre, GenreSchema, GenreManganime, GenreManganimeSchema } from './entities/genre.entity';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Genre.name, schema: GenreSchema }]),
    MongooseModule.forFeature([{ name: GenreManganime.name, schema: GenreManganimeSchema }]),
  ],
  controllers: [GenresController],
  providers: [GenresService]
})
export class GenresModule {}
