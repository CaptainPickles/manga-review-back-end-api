import {
    IsDate,
    IsDateString,
    IsEmail,
    isEmail,
    IsNumber,
    IsString,
    Matches,
    MaxLength,
    MinLength,
  } from 'class-validator';

/**
* This class is used to create / update the genre
*/
export class CreateGenreDto {
    /**
    * Name of the genre
    * @example "Action"
    */
    @IsString()
    @MinLength(4)
    @MaxLength(20)
    name: string;
}
