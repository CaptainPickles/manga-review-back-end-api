import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type GenreDocument = Genre & Document;
export type GenreManganimeDocument = GenreManganime & Document;

/**
 * This class is the genre entity used for mongoose
 */
@Schema({ collection: 'genres' })
export class Genre {
  /**
  * Name of the genre
  * @example "Action"
  */
  @Prop()
  name: string;
}

/**
 * This class is the genremanganime entity used for mongoose
 */
@Schema({ collection: 'genremanganimes' })
export class GenreManganime {
  /**
  * id of the genre
  * @example "62bd57d9f1be9ce48b3c9fbc"
  */
  @Prop({ref: 'Genre'})
  genreId: string;

  /**
  * id of the manganime
  * @example "62bd57d9f1be9ce48b3c9fbc"
  */
  @Prop({ref: 'Manganime'})
  manganimeId: string;
}


export const GenreSchema = SchemaFactory.createForClass(Genre);
export const GenreManganimeSchema = SchemaFactory.createForClass(GenreManganime);