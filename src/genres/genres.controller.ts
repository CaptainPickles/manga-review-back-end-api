import { UseGuards, Request, Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { GenresService } from './genres.service';
import { ApiResponse, ApiBearerAuth,ApiTags } from '@nestjs/swagger';
import { CreateGenreDto } from './dto/create-genre.dto';
import { UpdateGenreDto } from './dto/update-genre.dto';
import { JwtAuthGuard } from '../auth/authGuards/jwt-auth.guard';

/**
 * This class is the genre controller used to routing
 */
@ApiTags('genres')
@Controller('genres')
export class GenresController {
  constructor(private readonly genresService: GenresService) {}

  /**
   * Post request to create a genre
   * @example [POST] https://example/genres
   */
  @ApiResponse({ status: 201, description: 'The genre has been successfully created.'})
  @ApiResponse({ status: 400, description: 'Bad Request'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Request() req, @Body() createGenreDto: CreateGenreDto) {
    return this.genresService.create(req.user, createGenreDto);
  }

  /**
   * Get request to find all the genres
   * @example [GET] https://example/genres
   */
  @ApiResponse({ status: 200, description: 'The genres has been successfully requested.'})
  @Get()
  findAll() {
    return this.genresService.findAll();
  }

  /**
   * Get request to find one genre by its Id
   * @example [GET] https://example/genres/id
   */
  @ApiResponse({ status: 200, description: 'The genre has been successfully requested.'})
  @ApiResponse({ status: 400, description: 'Bad Request'})
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.genresService.findOne(id);
  }

  /**
   * Patch request to update one genre by its Id
   * @example [PATCH] https://example/genres/id
   */
  @ApiResponse({ status: 200, description: 'The genre has been successfully updated.'})
  @ApiResponse({ status: 400, description: 'Bad Request'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  update(@Request() req, @Param('id') id: string, @Body() updateGenreDto: UpdateGenreDto) {
    return this.genresService.update(req.user, id, updateGenreDto);
  }

  /**
   * Delete request to delete one genre by its Id
   * @example [DELETE] https://example/genres/id
   */
  @ApiResponse({ status: 200, description: 'The genre has been successfully deleted.'})
  @ApiResponse({ status: 400, description: 'Bad Request'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Request() req, @Param('id') id: string) {
    return this.genresService.remove(req.user, id);
  }
}
