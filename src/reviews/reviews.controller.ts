import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Request,
  Put,
} from '@nestjs/common';
import { ReviewsService } from './reviews.service';
import { UpsertReviewDto } from './dto/Upsert-review.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/authGuards/jwt-auth.guard';

/**
 * All route related to reviews
 */
@ApiTags('reviews')
@Controller('reviews')
export class ReviewsController {
  constructor(private readonly reviewsService: ReviewsService) { }

  /**
   * create/update a review for 1 user and 1 manganime
   * @param createReviewDto
   * @returns
   */
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Put()
  upsert(@Body() createReviewDto: UpsertReviewDto, @Request() req) {
    return this.reviewsService.upsert(createReviewDto, req.user._id);
  }

  @Get()
  findAll() {
    return this.reviewsService.findAll();
  }

  /**
   * find all reviews from 1 user
   * @param id pass id like so /reviews/user/62bc61eda526061b41a376d6
   */
  @Get('user/:id')
  findAllReviewsOfUser(@Param('id') id: string) {
    return this.reviewsService.findByUserId(id);
  }

  /**
   * Display top 10 most rated manganime 
   */
  @Get("Top10")
  findTop10MostRated() {
    return this.reviewsService.findTop10MostRated()
  }
}
