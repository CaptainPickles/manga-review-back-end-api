import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Date, Document } from 'mongoose';

export type ReviewDocument = Review & Document;

/**
 * Model used to create schema in mongoDb for review
 */
@Schema()
export class Review {
    @Prop()
    score: number;

    @Prop({ ref: 'User' })
    authorId: string;

    @Prop({ ref: 'Manganime' })
    manganimeId: string;

}

export const ReviewSchema = SchemaFactory.createForClass(Review);

