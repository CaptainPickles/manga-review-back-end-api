import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose, { Model } from 'mongoose';
import { UpsertReviewDto } from './dto/Upsert-review.dto';
import { Review, ReviewDocument } from './entities/review.entity';

@Injectable()
export class ReviewsService {
  constructor(
    @InjectModel(Review.name) private reviewModel: Model<ReviewDocument>,
  ) { }
  async upsert(createReviewDto: UpsertReviewDto, userId: string) {
    if (
      !mongoose.Types.ObjectId.isValid(userId) ||
      !mongoose.Types.ObjectId.isValid(createReviewDto.manganimeId)
    ) {
      throw new BadRequestException('id not good format');
    }
    const alreadyExistingReview = await this.reviewModel.findOne({
      authorId: userId,
      manganimeId: createReviewDto.manganimeId,
    });
    let id: string = '';
    if (alreadyExistingReview !== null) {
      await alreadyExistingReview.updateOne({ score: createReviewDto.score });
      id = alreadyExistingReview.id;
    } else {
      const reviewToCreate: Review = Object.assign(
        { authorId: userId },
        createReviewDto,
      );
      const manganimeToCreate = new this.reviewModel(reviewToCreate);
      const createdReview = await manganimeToCreate.save();
      id = createdReview.id;
    }

    return id;
  }

  async findAll() {
    const reviews = await this.reviewModel
      .find()
      .populate('authorId', '-password')
      .populate('manganimeId');
    return reviews;
  }

  async findByUserId(authorId: string) {
    if (!mongoose.Types.ObjectId.isValid(authorId)) {
      throw new BadRequestException('authorId not good format');
    }
    const reviewsFromUser = await this.reviewModel
      .find({ authorId: authorId })
      .populate('authorId', '-password')
      .populate('manganimeId');

    return reviewsFromUser;
  }

  async findTop10MostRated() {
    return this.reviewModel.find().sort({ score: "descending" }).limit(10).populate('authorId', '-password')
      .populate('manganimeId');
  }
}
