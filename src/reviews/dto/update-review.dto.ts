import { PartialType } from '@nestjs/swagger';
import { UpsertReviewDto } from './Upsert-review.dto';

export class UpdateReviewDto extends PartialType(UpsertReviewDto) { }
