import { IsInt, IsString, Max, Min } from "class-validator";
/**
 * Dto used to update / modify the review
 * Only used for transport
 */
export class UpsertReviewDto {
    /**
     * Score of the manganime 
     * - 1 user can only have one score per Manganime
     * @example 4
     */
    @IsInt()
    @Min(0)
    @Max(5)
    score: number

    /**
     * Manganime Id ref of Manganime table
     * @example '5d6ede6a0ba62570afcedd3a'
     */
    @IsString()
    manganimeId: string
}
