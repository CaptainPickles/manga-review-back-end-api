import { Body, Controller, Delete, Get, Param, Patch, Post, Put, Query, Request, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/authGuards/jwt-auth.guard';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdatePasswordUserDto } from './dto/update-password.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserDocument } from './entities/user.entity';
import { UsersService } from './users.service';
/**
 * All route related to users
 */
@ApiTags('users')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) { }
  /**
   * Allow registration of a new user
   */
  @Post('register')
  signUp(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  /**
 * Allow modification of a user
 */
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Patch('update')
  updateUserInfo(@Body() updateUserDto: UpdateUserDto, @Request() req) {
    return this.usersService.updateUser(updateUserDto, req.user._id);
  }

  /**
* allow modification of a pass
*/
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Patch('password')
  updatePassword(@Body() updatePasswordDto: UpdatePasswordUserDto, @Request() req) {
    return this.usersService.updatePassword(updatePasswordDto, req.user._id);
  }
  /**
   * List all user stored in db
   * - only member not admin
   */
  @Get("list")
  listUser() {
    return this.usersService.listUsers()
  }

  /**
   * Delete user only if :
   * - you are an admin and
   * - you don"t delete yourself
   */
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Delete()
  deleteUser(@Query() query: { id: string }, @Request() req) {
    return this.usersService.deleteUser(query.id, req.user)
  }

  @Get("id/:id")
  getUserById(@Param('id') id: string) {
    return this.usersService.getUserById(id)
  }
}
