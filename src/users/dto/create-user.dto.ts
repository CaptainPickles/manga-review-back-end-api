import {
  IsBoolean,
  isBoolean,
  IsDate,
  IsDateString,
  IsEmail,
  isEmail,
  IsNumber,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';

/**
 * Only used for transport
 */
export class CreateUserDto {
  /**
   * strong password Must contain 1 Maj and 1 number
   * @example "dqz16516DEd"
   */
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'password too weak',
  })
  password: string;

  /**
   * Email used to create unique user
   * @example "michel.test@gmail.com"
   */
  @IsEmail()
  email: string;

  /**
   * username used in website
   * @example "Jacque150"
   */
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  username: string;
}
