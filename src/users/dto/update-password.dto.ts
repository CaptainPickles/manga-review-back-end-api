import {
    IsBoolean,
    isBoolean,
    IsDate,
    IsDateString,
    IsEmail,
    isEmail,
    IsNumber,
    IsString,
    Matches,
    MaxLength,
    MinLength,
} from 'class-validator';

/**
 * Password modification dto when you know your old password
 */
export class UpdatePasswordUserDto {
    /**
     * old Password of logged in user
     * strong password Must contain 1 Maj and 1 number
     * @example "dqz16516DEd"
     */
    @IsString()
    @MinLength(4)
    @MaxLength(20)
    @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
        message: 'password too weak',
    })
    oldPassword: string;

    /**
     * new password of logged in user
     * strong password Must contain 1 Maj and 1 number
     * @example "AqD21566d"
     */
    @IsString()
    @MinLength(4)
    @MaxLength(20)
    @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
        message: 'password too weak',
    })
    newPassword: string;
}
