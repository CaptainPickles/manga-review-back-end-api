import { PartialType } from '@nestjs/swagger';
import { IsEmail, IsOptional, IsString, IsUrl, MaxLength, MinLength } from 'class-validator';
/**
 * Dto used to modify user
 */
export class UpdateUserDto {
    /**
 * Email used to create unique user
 * @example "michel.test@gmail.com"
 */
    @IsEmail()
    email: string;

    /**
     * username used in website
     * @example "Jacque150"
     */
    @IsString()
    @MinLength(4)
    @MaxLength(20)
    username: string;

    /**
     * profile description
     * @example "i like naruto a lot"
     */
    @IsString()
    @MinLength(1)
    @MaxLength(500)
    description: string;

    /**
     * image user profile 
     * - if not specified will be sent in front
     * - if specified must be an url
     * @example "https://www.garonapromotion.fr/wp-content/uploads/sites/4/2017/12/Image-test-1_large.jpg"
     */
    @IsString()
    @IsOptional()
    @IsUrl()
    imageUrl?: string;
}
