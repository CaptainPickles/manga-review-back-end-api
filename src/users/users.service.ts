import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { randomBytes, scrypt as _scrypt } from 'crypto';
import mongoose, { Model } from 'mongoose';
import { promisify } from 'util';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdatePasswordUserDto } from './dto/update-password.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User, UserDocument } from './entities/user.entity';

const scrypt = promisify(_scrypt);

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async create(createUserDto: CreateUserDto): Promise<Partial<User>> {
    const user = await this.userModel.findOne({ email: createUserDto.email });
    if (user !== null) {
      throw new BadRequestException('email already used');
    }

    const salt = randomBytes(8).toString('hex');

    const hash = (await scrypt(createUserDto.password, salt, 32)) as Buffer;

    const result = salt + '.' + hash.toString('hex');
    createUserDto.password = result;

    const userToCreate = new this.userModel(createUserDto);
    const createdUser = await userToCreate.save();
    const userToReturn = createdUser.toJSON();
    delete userToReturn.password;
    return userToReturn;
  }

  async updateUser(updateUserDto: UpdateUserDto, userId: string) {
    const user = await this.userModel.findOne({ email: updateUserDto.email });
    if (user !== null && user.id !== userId) {
      throw new BadRequestException('email already used');
    }
    const updatedUser = await this.userModel.findByIdAndUpdate(
      userId,
      {
        description: updateUserDto.description,
        email: updateUserDto.email,
        username: updateUserDto.username,
        imageUrl: updateUserDto.imageUrl,
      },
      { new: true },
    ); // return new  updated user
    const userToReturn = updatedUser.toJSON();
    delete userToReturn.password;
    return userToReturn;
  }

  async updatePassword(
    updatePasswordDto: UpdatePasswordUserDto,
    userId: string,
  ) {
    const user = await this.userModel.findById(userId);
    const [salt, storedHash] = user.password.split('.');
    const hash = (await scrypt(
      updatePasswordDto.oldPassword,
      salt,
      32,
    )) as Buffer;
    if (storedHash !== hash.toString('hex')) {
      throw new BadRequestException('not the same password');
    }

    const newSalt = randomBytes(8).toString('hex');

    const newHash = (await scrypt(
      updatePasswordDto.newPassword,
      newSalt,
      32,
    )) as Buffer;

    const result = newSalt + '.' + newHash.toString('hex');

    const updatedUser = await this.userModel.findByIdAndUpdate(
      userId,
      {
        password: result,
      },
      { new: true },
    ); // return new  updated user

    const userToReturn = updatedUser.toJSON();
    delete userToReturn.password;
    return userToReturn;
  }

  async findOne(id: string): Promise<UserDocument> {
    if (!id.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('id not good format');
    }

    const user = await this.userModel.findById(id);
    if (!user) {
      throw new NotFoundException('Could not find user'); // should i return undefined
    }
    return user;
  }

  async findByEmail(email: string): Promise<UserDocument> {
    if (!email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
      throw new BadRequestException('email is not valid');
    }
    const user = await this.userModel.findOne({ email: email });
    if (!user) {
      throw new NotFoundException('Could not find user');
    }
    return user;
  }
  /**
   *
   * @returns all users in db without their password
   */
  async listUsers() {
    const users = await this.userModel
      .find({ isAdmin: false })
      .select(['-password']);
    return users;
  }
  /**
   * delete user if you are admin and you don t delete yourself
   * @return true if user has been deleted
   */
  async deleteUser(idUserToDelete: string, user: UserDocument): Promise<true> {
    if (idUserToDelete === undefined) {
      throw new BadRequestException('no id provided');
    }
    if (!idUserToDelete.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('id not good format');
    }
    if (!user.isAdmin) {
      throw new UnauthorizedException('You must be admin to delete user');
    }
    if (user._id === idUserToDelete) {
      throw new BadRequestException("You canno't delete yourself");
    }

    await this.userModel.findByIdAndDelete(idUserToDelete);

    return true;
  }

  async getUserById(userId: string) {
    if (!mongoose.Types.ObjectId.isValid(userId)) {
      throw new BadRequestException('id not good format');
    }

    const user = await this.userModel.findById(userId).select(['-password']);
    return user;
  }
}
