import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Model } from 'mongoose';
import { User, UserDocument } from './entities/user.entity';
import { UsersService } from './users.service';

describe('UsersService', () => {
  let service: UsersService;
  const userSeededInDb: Partial<UserDocument> = {
    _id: '62b997c7c9a7d7383c1c2bf6',
    email: 'test@test.fr',
    password:
      'ca8e643748acb5ba.94e622aca113c37756693b0ccbe5014d5474d122733ac900d8492bf285153ec5', // hashed and salted Test123
    username: 'test',
    imageUrl: "https://www.garonapromotion.fr/wp-content/uploads/sites/4/2017/12/Image-test-1_large.jpg"
  };

  beforeEach(async () => {
    const fakeBddUsers: Partial<UserDocument>[] = [userSeededInDb];
    const mockUserModel = {
      findById: (id: any) => {
        const findUser = fakeBddUsers.find(
          (user) => user._id === id,
        ) as Partial<UserDocument>;
        return Promise.resolve(findUser);
      },
      findOne: (query: any) => {
        const findUser = fakeBddUsers.find(
          (user) => user.email === query.email,
        ) as Partial<UserDocument>;
        return Promise.resolve(findUser);
      },
    };
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        { provide: getModelToken('User'), useValue: mockUserModel },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should throw error if wrong id findOne', async () => {
    expect(service.findOne('id not supported by mongodb')).rejects.toThrow(
      'id not good format',
    );
  });

  it('should throw error if user id not exist findOne', async () => {
    expect(service.findOne('62b997c7c9a7d7383c1c2bf7')).rejects.toThrow(
      'Could not find user',
    );
  });

  it('should return user findOne', async () => {
    const foundUser = await service.findOne('62b997c7c9a7d7383c1c2bf6');
    expect(foundUser).toMatchObject(userSeededInDb);
  });

  it('should throw error if email not good format findByEmail', async () => {
    expect(service.findByEmail('email not good format')).rejects.toThrow(
      'email is not valid',
    );
  });

  it('should throw error if email not exist findByEmail', async () => {
    expect(service.findByEmail('notSavedEmail@gmail.com')).rejects.toThrow(
      'Could not find user',
    );
  });
  it('should return user findByEmail', async () => {
    const foundUser = await service.findByEmail(userSeededInDb.email);
    expect(foundUser).toMatchObject(userSeededInDb);
  });

  it.todo('test create method');
});
